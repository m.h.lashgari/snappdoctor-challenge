import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import Header from "../Header";

jest.mock("next/navigation", () => {
  return {
    __esModule: true,
    usePathname: () => ({ pathname: "" }),
    useRouter: () => ({
      push: jest.fn(),
      replace: jest.fn(),
      prefetch: jest.fn(),
    }),
    useSearchParams: () => ({ get: jest.fn(() => "") }),
    useServerInsertedHTML: jest.fn(),
  };
});

describe("Header component", () => {
  const user = userEvent.setup();

  it("renders header with input and search button", () => {
    render(<Header />);

    const inputElement = screen.getByRole("textbox");
    const searchButton = screen.getByRole("button");

    expect(inputElement).toBeInTheDocument();
    expect(searchButton).toBeInTheDocument();
  });

  it("handles input value changes", async () => {
    render(<Header />);

    const inputElement = screen.getByRole("textbox");
    await user.type(inputElement, "search query");

    expect(inputElement).toHaveValue("search query");
  });
});
