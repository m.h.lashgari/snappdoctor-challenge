const Footer = () => {
  return (
    <footer className="text-center border p-2 rounded-lg  my-4 shadow-lg">
      <h3>Snapp doctor challenge by M.H.Lashgari</h3>
    </footer>
  );
};

export default Footer;
