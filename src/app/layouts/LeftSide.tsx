import Link from "next/link";
import Drawer from "../components/views/Drawer";

async function getCategories() {
  const res = await fetch(`https://dummyjson.com/products/categories`);
  return res.json();
}

const LeftSide = async () => {
  const categories = await getCategories();

  return (
    <Drawer drawerTitle="categories">
      <section className="flex flex-col border rounded-lg  shadow-lg p-4 min-w-[185px] mr-4">
        <h2 className="py-2">categories</h2>
        <Link
          className="border my-2 text-sm rounded-3xl p-1 text-center hover:text-red-700 "
          href={`/products`}
        >
          Remove all filters
        </Link>

        {categories.map((category: string) => (
          <Link
            className="hover:text-green-800 border-t-2 py-1"
            href={`/products?category=${category}`}
            key={category}
          >
            {category}
          </Link>
        ))}
      </section>
    </Drawer>
  );
};

export default LeftSide;
