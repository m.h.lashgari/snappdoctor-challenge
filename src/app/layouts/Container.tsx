import Footer from "./Footer";
import Header from "./Header";
import LeftSide from "./LeftSide";

const Container = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className="mx-auto max-w-screen-2xl p-4 ">
      <Header />
      <div className="flex flex-col md:flex-row items-center md:items-start">
        <div className=" md:w-1/4">
          {/* @ts-ignore */}
          <LeftSide />
        </div>
        <div className="md:w-3/4 w-full"> {children}</div>
      </div>
      <Footer />
    </div>
  );
};

export default Container;
