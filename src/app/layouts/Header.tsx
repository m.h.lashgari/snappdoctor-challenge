"use client";

import Link from "next/link";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useState } from "react";
import Button from "../components/views/Button";
import Input from "../components/views/Input";

const Header = () => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  const [inputValue, setInputValue] = useState("");

  // * We can only handle it with * debounce * - when user change value on input after seconds add new value

  // const handleInputValueDebounced = debounce((value) => {
  //   const currentParams = new URLSearchParams(Array.from(searchParams.entries()));
  //   currentParams.set("q", value);
  //   const search = currentParams.toString();
  //   const query = search ? `?${search}` : "";
  //   router.push(`${pathname}${query}`);
  // }, 300);

  const handleInputValue = (value: string) => {
    setInputValue(value);
  };

  const handleSearchButton = () => {
    const currentParams = new URLSearchParams(
      Array.from(searchParams.entries())
    );
    currentParams.delete("category");
    currentParams.set("q", inputValue);
    currentParams.set("page", "1");

    if (inputValue === "") {
      currentParams.delete("q");
    }
    const search = currentParams.toString();
    const query = search ? `?${search}` : "";

    router.push(`/products${query}`);
  };

  const handleButtonOnKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      handleSearchButton();
    }
  };

  return (
    <header className="p-4 border my-3 rounded-lg shadow-md text-center">
      <div className="flex bg-transparent justify-center max-w-xl mx-auto">
        <Input
          onInputChange={handleInputValue}
          defaultValue={searchParams.get("q") ?? ""}
          onKeyDown={handleButtonOnKeyDown}
          // customClassName="w-10"
        >
          <Button
            customClassName="bg-transparent t-0 right-0 absolute"
            label="&#x1F50D;"
            onClick={handleSearchButton}
          />
        </Input>
      </div>
    </header>
  );
};

export default Header;
