import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import Pagination from "../Pagination";

jest.mock("next/navigation", () => {
  return {
    __esModule: true,
    usePathname: () => ({ pathname: "" }),
    useRouter: () => ({
      push: jest.fn(),
      replace: jest.fn(),
      prefetch: jest.fn(),
    }),
    useSearchParams: () => ({ get: () => {} }),
    useServerInsertedHTML: jest.fn(),
  };
});

describe("Pagination component", () => {
  const defaultProps = {
    totalPages: 5,
    limit: 1,
  };

  it("renders pagination buttons with correct styles", () => {
    render(<Pagination {...defaultProps} />);

    const paginationButtons = screen.getAllByRole("button");

    expect(paginationButtons).toHaveLength(defaultProps.totalPages);

    paginationButtons.forEach((button, index) => {
      const pageNumber = index + 1;

      expect(button).toHaveTextContent(pageNumber.toString());

      if (pageNumber === 1) {
        expect(button).toHaveClass("bg-indigo-600");
        expect(button).toHaveClass("text-white");
      } else {
        expect(button).toHaveClass("text-gray-900");
        expect(button).toHaveClass("hover:bg-gray-50");
      }
    });
  });

  it("updates the Pagination with the correct page parameter when a button is clicked", () => {
    render(<Pagination {...defaultProps} />);

    const paginationButtons = screen.getAllByRole("button");

    userEvent.click(paginationButtons[2]);

    expect(paginationButtons[2]).toHaveClass(
      "relative text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 px-[10px] md:px-4 py-2 text-sm font-semibold false ring-1 ring-inset ring-gray-300 focus:outline-offset-0"
    );
  });
});
