import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Input from "../Input";
import userEvent from "@testing-library/user-event";

describe("Input component", () => {
  const user = userEvent.setup();

  const defaultProps = {
    labelName: "Username",
    onInputChange: jest.fn(),
    value: "controlledValue",
    customClassName: "custom-input",
  };

  const renderInput = () => {
    render(<Input {...defaultProps} />);
  };

  it("renders input with correct label and className", () => {
    renderInput();

    const inputElement = screen.getByRole("textbox");

    expect(inputElement).toBeInTheDocument();
    expect(inputElement).toHaveValue(defaultProps.value);
    expect(inputElement).toHaveAttribute("id", defaultProps.labelName);
    expect(inputElement).toHaveAttribute("value", defaultProps.value);
    expect(inputElement).toHaveClass("custom-input");
    expect(screen.getByLabelText(defaultProps.labelName!)).toBeInTheDocument();
  });

  it("calls the onInputChange function when the input value changes", async () => {
    renderInput();

    const inputElement = screen.getByRole("textbox");
    fireEvent.change(inputElement, { target: { value: "newInputValue" } });

    expect(defaultProps.onInputChange).toHaveBeenCalledWith("newInputValue");
  });
});
