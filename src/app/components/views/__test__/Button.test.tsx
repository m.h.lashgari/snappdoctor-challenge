import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";
import Button from "../Button";

describe("Button component", () => {
  const user = userEvent.setup();

  const defaultProps = {
    label: "Click me",
    onClick: jest.fn(),
    customClassName: "custom-button",
    type: "button",
  };

  const renderButton = () => {
    render(
      <Button
        label={defaultProps.label}
        onClick={defaultProps.onClick}
        customClassName={defaultProps.customClassName}
        type={"submit"}
      />
    );
  };

  it("renders button with correct props", () => {
    renderButton();

    const buttonElement = screen.getByRole("button");

    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement).toHaveTextContent(defaultProps.label);
    expect(buttonElement).toHaveClass("custom-button");
  });

  it("calls the onClick function when the button is clicked", async () => {
    renderButton();

    const buttonElement = screen.getByRole("button");

    await user.click(buttonElement);

    expect(defaultProps.onClick).toHaveBeenCalledTimes(1);
  });
});
