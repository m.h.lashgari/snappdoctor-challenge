"use client";

import React, { useCallback } from "react";
import { usePathname, useRouter, useSearchParams } from "next/navigation";

interface IProps {
  totalPages: number;
  limit: number;
}

const Pagination = ({ totalPages, limit }: IProps) => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams()!;

  const createQueryString = useCallback(
    (name: string, value: string) => {
      const params = new URLSearchParams(searchParams);
      params.set(name, value);

      return params.toString();
    },
    [searchParams]
  );

  const handlePage = (page: string) => {
    router.push(pathname + "?" + createQueryString("page", page));
  };

  const activePage = searchParams.get("page")
    ? Number(searchParams.get("page"))
    : 1;

  return (
    <div className="flex items-center justify-between  bg-white px-4 py-3 sm:px-6">
      <div className=" sm:flex sm:flex-1 sm:items-center sm:justify-between">
        <div>
          <nav
            className="isolate inline-flex -space-x-px rounded-md shadow-sm"
            aria-label="Pagination"
          >
            {Array.from(Array(Math.ceil(totalPages / limit)).keys()).map(
              (page) => (
                <button
                  className={`relative ${
                    page == activePage - 1
                      ? "z-10 bg-indigo-600 text-white focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                      : "text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                  } px-[10px] md:px-4 py-2 text-sm font-semibold ${
                    page == activePage - 1 && "text-white bg-indigo-600"
                  } ring-1 ring-inset ring-gray-300 focus:outline-offset-0`}
                  key={page}
                  onClick={() => handlePage(String(page + 1))}
                >
                  {page + 1}
                </button>
              )
            )}
          </nav>
        </div>
      </div>
    </div>
  );
};

export default Pagination;
