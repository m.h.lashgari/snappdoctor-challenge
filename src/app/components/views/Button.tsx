"use client";
import React, { ComponentProps } from "react";

interface IProps extends ComponentProps<"button"> {
  label: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  customClassName?: string;
  type?: "button" | "submit" | "reset" | undefined;
}

const Button = ({
  label,
  onClick,
  customClassName,
  type,
  ...props
}: IProps) => {
  return (
    <button
      {...props}
      type={type}
      className={`${customClassName} bg-blue-500 text-white px-4 py-2 rounded-md whitespace-nowrap `}
      onClick={onClick}
    >
      {label}
    </button>
  );
};

export default Button;
