import { ChangeEvent, ComponentProps } from "react";

interface IProps extends ComponentProps<"input"> {
  labelName?: string;
  onInputChange: (value: string) => void;
  defaultValue?: string;
  value?: string;
  customClassName?: string;
  children?: React.ReactNode;
}

const Input = ({
  labelName,
  onInputChange,
  value,
  defaultValue,
  customClassName,
  children,
  ...props
}: IProps) => {
  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    onInputChange(value);
  };
  return (
    <div className="relative flex items-center w-full ">
      {labelName ? <label htmlFor={labelName}>{labelName}</label> : null}
      <input
        {...props}
        className={`w-full mx-2 border-2 rounded-md p-1  ${customClassName}`}
        id={labelName}
        onChange={handleInputChange}
        defaultValue={defaultValue}
        value={value}
      />
      {children}
    </div>
  );
};

export default Input;
