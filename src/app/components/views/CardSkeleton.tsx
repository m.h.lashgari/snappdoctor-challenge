import Image from "next/image";
import React from "react";

const CardSkeleton = () => {
  return (
    <div
      role="status"
      className=" p-4 w-full flex flex-col border border-gray-200 rounded-lg shadow animate-pulse md:p-6 dark:border-gray-300"
    >
      <div className="flex w-full items-center justify-center h-56 mb-4 bg-gray-300 rounded dark:bg-gray-300">
        {/* Image or icon placeholder */}
        <Image src={""} width={300} height={300} alt="" className="invisible" />
      </div>
      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-300 mb-2.5"></div>
      <div className="flex items-center mt-4 space-x-3"></div>
    </div>
  );
};

export default CardSkeleton;
