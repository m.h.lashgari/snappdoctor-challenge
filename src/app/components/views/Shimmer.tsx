export const shimmer = (w: number, h: number) => `
<svg width="${w}" height="${h}" xmlns="http://www.w3.org/2000/svg">
  <rect width="${w}" height="${h}" fill="#E5E7EB" />
  <rect width="100%" height="56" fill="#F3F4F6" />
  <rect width="48" height="2.5" rx="1.25" y="70" fill="#F3F4F6" />
  <rect width="100%" height="2" y="80" fill="#F3F4F6" />
  <rect width="100%" height="2" y="85" fill="#F3F4F6" />
</svg>
`;
