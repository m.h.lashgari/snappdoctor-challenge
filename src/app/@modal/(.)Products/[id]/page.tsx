import Frame from "@/app/products/[id]/components/ProductDetail";
import Modal from "@/app/components/views/Modal";
import ProductDetail from "@/app/products/[id]/components/ProductDetail";

async function getProductDetails(id: string) {
  const res = await fetch(`https://dummyjson.com/products/${id}`, {
    next: { revalidate: 20 },
  });
  return res.json();
}

const ProductDetails = async ({
  params: { id },
}: {
  params: { id: string };
}) => {
  const productDetails = await getProductDetails(id);

  return (
    <Modal>
      <ProductDetail productItem={productDetails} />
    </Modal>
  );
};

export default ProductDetails;
