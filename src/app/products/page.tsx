import Pagination from "@/app/components/views/Pagination";
import { TProduct } from "../types";
import Product from "./components/ProductCard";

interface IProps {
  searchParams: { category: string; q: string; page: string };
}

const getProductsByCategory = async (category: string) => {
  const res = await fetch(
    `https://dummyjson.com/products/category/${category}`,
    { next: { revalidate: 3000 } }
  );
  return res.json();
};

const getProductsBySearch = async (q: string, page: string) => {
  const res = await fetch(
    `https://dummyjson.com/products/search?q=${q}&limit=10&skip=${
      page ? Number(page) * 10 - 10 : 1
    }`,
    {
      next: { revalidate: 3000 },
    }
  );
  return res.json();
};

const getAllProducts = async (page: string) => {
  const res = await fetch(
    `https://dummyjson.com/products?limit=10&skip=${
      page ? Number(page) * 10 - 10 : 1
    }`,
    {
      next: { revalidate: 3000 },
    }
  );
  return res.json();
};

const Products = async ({ searchParams }: IProps) => {
  let getData;
  if (searchParams.category) {
    getData = await getProductsByCategory(searchParams.category);
  }
  if (searchParams.q) {
    getData = await getProductsBySearch(searchParams.q, searchParams.page);
  }
  if (!searchParams.q && !searchParams.category) {
    getData = await getAllProducts(searchParams.page);
  }

  return (
    <>
      <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
        {getData?.products?.map((product: TProduct) => (
          <Product productItem={product} key={product.id} />
        ))}
      </div>
      {getData.total >= 10 ? (
        <div className="flex justify-center my-3">
          <Pagination totalPages={getData.total} limit={10} />
        </div>
      ) : null}
    </>
  );
};

export default Products;
