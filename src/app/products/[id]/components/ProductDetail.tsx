import Image from "next/image";
import { TProduct } from "../../../types";

export default function ProductDetail({
  productItem,
}: {
  productItem: TProduct;
}) {
  return (
    <>
      <Image
        alt={productItem.title}
        src={productItem?.thumbnail}
        height={600}
        width={900}
        className="w-full object-cover aspect-square col-span-2"
      />

      <div className="bg-white p-4 px-6">
        <h3>{productItem?.title}</h3>
        <h4 className="text-sm font-medium ">{productItem?.price}$</h4>
        <p>{productItem?.description}</p>
      </div>
    </>
  );
}
