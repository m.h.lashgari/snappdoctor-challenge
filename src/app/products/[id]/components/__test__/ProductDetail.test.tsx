import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import ProductDetail from "../ProductDetail";

describe("Product Detail component rendering", () => {
  const productItem = {
    id: 10,
    title: "HP Pavilion 15-DK1056WM",
    description:
      "HP Pavilion 15-DK1056WM Gaming Laptop 10th Gen Core i5, 8GB, 256GB SSD, GTX 1650 4GB, Windows 10",
    price: 1099,
    discountPercentage: 6.18,
    rating: 4.43,
    stock: 89,
    brand: "HP Pavilion",
    category: "laptops",
    thumbnail: "https://i.dummyjson.com/data/products/10/thumbnail.jpeg",
    images: [
      "https://i.dummyjson.com/data/products/10/1.jpg",
      "https://i.dummyjson.com/data/products/10/2.jpg",
      "https://i.dummyjson.com/data/products/10/3.jpg",
      "https://i.dummyjson.com/data/products/10/thumbnail.jpeg",
    ],
  };

  it("should render the product details correctly", async () => {
    render(<ProductDetail productItem={productItem} />);

    const titleElement = screen.getByRole("heading", {
      name: productItem.title,
    });

    const imageElement = screen.getByAltText(productItem.title);

    const priceElement = screen.getByText(
      new RegExp(`${productItem.price}\\$`, "i")
    );

    expect(titleElement).toBeInTheDocument();
    expect(imageElement).toBeInTheDocument();
    expect(priceElement).toBeInTheDocument();
  });
});
