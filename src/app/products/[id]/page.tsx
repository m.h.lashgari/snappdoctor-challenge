import ProductDetail from "@/app/products/[id]/components/ProductDetail";
import Frame from "@/app/products/[id]/components/ProductDetail";

async function getProductDetails(id: string) {
  const res = await fetch(`https://dummyjson.com/products/${id}`, {
    next: { revalidate: 20 },
  });
  return res.json();
}

const ProductDetails = async ({
  params: { id },
}: {
  params: { id: string };
}) => {
  const productDetails = await getProductDetails(id);

  return (
    <div className="container mx-auto my-10">
      <div className="max-w-[500px] mx-auto  border-gray-700">
        <ProductDetail productItem={productDetails} />
      </div>
    </div>
  );
};

export default ProductDetails;
