import CardSkeleton from "../components/views/CardSkeleton";

export default function Loading() {
  return (
    <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
      {[1, 2, 3, 4, 5, 6, 7, 8].map((i) => {
        return <CardSkeleton key={i} />;
      })}
    </div>
  );
}
