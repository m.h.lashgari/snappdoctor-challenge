import { TProduct } from "@/app/types";
import { shimmer } from "@/app/components/views/Shimmer";
import { toBase64 } from "@/app/utility/toBase64";
import Image from "next/image";
import Link from "next/link";

const ProductCard = ({ productItem }: { productItem: TProduct }) => {
  return (
    <div
      key={productItem.id}
      className="group relative cursor-pointer shadow-lg border p-2 rounded-lg flex flex-col"
    >
      <Link href={`/products/${productItem.id}`} scroll={false}>
        <div className="aspect-h-1  w-full aspect-w-1 relative max-h-[200px]  overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75  h-96 mb-2">
          <Image
            className="object-fill"
            src={productItem?.thumbnail}
            alt={productItem?.title}
            fill
            placeholder="blur"
            blurDataURL={`data:image/svg+xml;base64,${toBase64(
              shimmer(500, 500)
            )}`}
          />
        </div>
        <div className="mt-auto flex justify-between ">
          <div>
            <h3 className="text-sm text-gray-700">
              <span aria-hidden="true" className="absolute inset-0" />
              {productItem.title}
            </h3>
          </div>
          <h4 className="text-sm font-medium text-gray-900">
            {productItem?.price}$
          </h4>
        </div>
      </Link>
    </div>
  );
};

export default ProductCard;
