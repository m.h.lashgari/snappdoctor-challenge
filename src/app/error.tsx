"use client";

import { useEffect } from "react";

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  useEffect(() => {
    // Log the error to an error reporting service
    console.error(error);
  }, [error]);

  return (
    <div>
      <h2>
        Ops,Something went wrong. or maybe you need to turn on your VPN :)
      </h2>
      <button
        onClick={() => reset()}
        className="border shadow-lg p-4 rounded-lg"
      >
        Try again
      </button>
    </div>
  );
}
