import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Container from "./layouts/Container";

const inter = Inter({ subsets: ["latin"] });

// or we can add useful metadata for better SEO
export const metadata: Metadata = {
  title: "snapp doctor challenge",
  description: "Generated by  MHL for snapp doctor challenge",
};

export default function Layout(props: {
  children: React.ReactNode;
  modal: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        {props.modal}
        <Container>{props.children}</Container>
      </body>
    </html>
  );
}
