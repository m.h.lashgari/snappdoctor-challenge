import React, { ReactElement } from "react";
import { render, RenderOptions } from "@testing-library/react";

// ** We can have custom wrapper - for example config Theme or redux or react-route-dom or ...
// ** Also we can mock api with MSW (mock service worker)

// const AllTheProviders = ({ children }: { children: React.ReactNode }) => {
//   return (
//     <ThemeProvider theme="light">
//       <TranslationProvider messages={defaultStrings}>
//         {children}
//       </TranslationProvider>
//     </ThemeProvider>
//   );
// };

// const customRender = (
//   ui: ReactElement,
//   options?: Omit<RenderOptions, "wrapper">
// ) => render(ui, { wrapper: AllTheProviders, ...options });

// export * from "@testing-library/react";
// export { customRender as render };
