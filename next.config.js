/** @type {import('next').NextConfig} */

// ** Here we can have some config for next.js project like redirects or ..
const nextConfig = {
  images: {
    domains: ["i.dummyjson.com"],
  },
  async redirects() {
    return [
      {
        source: "/",
        destination: "/products",
        permanent: false,
      },
    ];
  },
};

module.exports = nextConfig;
