To start the development server, use the following commands:

```bash
npm run dev
# or
npm run start
```

To run the tests, use the following command:
```bash
npm run test
```

The project is deployed on Vercel and can be accessed at the following link: https://snappdoctor-challenge.vercel.app/products.
**Please use a VPN to check it.**

In this project:

1. All data fetching is managed server-side to enhance SEO and speed up data retrieval.
2. Styling is done using Tailwind.
3. Parallel routing is implemented to handle modals for product detail viewing.
4. Jest and the React testing library are utilized for testing.




